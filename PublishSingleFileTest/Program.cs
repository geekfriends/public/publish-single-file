﻿using System;

namespace PublishSingleFileTest
{
    class Program
    {
        static void Main(string[] args)
        {
            // For windows: dotnet publish -r win-x64 -c Release /p:PublishSingleFile=true
            // For linux: dotnet publish -r linux-x64 -c Release /p:PublishSingleFile=true
            Console.WriteLine("PublishSingleFile Test!");
        }
    }
}
